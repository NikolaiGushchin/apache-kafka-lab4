package org.lab4.producer;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Date;
import java.util.Properties;

public class JavaKafkaProducer {
    public static void main(String[] args) {
        final String server = "localhost:9092";
        final String topic = "lab4-messages";
        final String key = " message";
        final long numMsg = 1000L;

        final Properties props = new Properties();

        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, server);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());


        try (final Producer<String, String> producer = new KafkaProducer<>(props)) {
            String value = new Date().toString();

            for (long i = 0L; i <= numMsg; i++) {
                producer.send(
                    new ProducerRecord<>(topic, i + key, value)
                );
            }

            System.out.println("Messages send");

            producer.flush();
        }
    }
}
